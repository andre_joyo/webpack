//ARROW FUNCTION
//hello = (val) => { "Hello " + val; }  //hello = (val) => "Hello " + val;
var hello = val => "Arrow " + val;
console.log(hello('function'));

//CLASES ES6
export class Car {
    constructor(brand) {
      this.carname = brand;
    }
    present(x) {
      return x + ", I have a " + this.carname;
    }
}

//TEMPLATE STRINGS
export function funTemplateStrings() {
  var myTemplateString = 'La donna e mobile cual piuma al vento';
  console.info( `String value: ${ myTemplateString }` );  
}

//LET y CONST
var x = 10;
console.log(x);// Here x is 10
const y = 20;
console.log(y);// Here y is 20
{
  let x = 2;
  console.log(x);// Here x is 2
  const y = 7;
  console.log(y);// Here y is 7
}
console.log(x);// Here x is 10
console.log(y);// Here y is 20
