const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports={
	entry: './src/main.js',
	output:{
		path: __dirname + '/build',
		filename: 'bundle.js'
	},
	module: {
		rules: [
		  	{
				test: /\.scss$/i,
				use: [
					{loader: 'style-loader'},
					// Translates CSS into CommonJS
					{loader: 'css-loader'},
					// Compiles Sass to CSS
					{loader: 'sass-loader'},
				]
			},
			{
				test: /\.(png|jpg|gif)$/i,
				use: [
				  	{
						loader: 'url-loader',
						options: {
					  		limit: 8192,
						},
				  	},
				]
			},
			{
				test: /\.(png|jpg|gif)$/i,
				use: [
				  	{
						loader: 'file-loader',
						options: {
							  name: '[name].[ext]',
							  outputPath: 'static/',
							  useRelativePath: true
						},
				  	},
				]
		  	}
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'src/index.html'
		})
	]
}